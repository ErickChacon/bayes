
# Load custom scripts

include(joinpath("src", "files.jl"))

# Update Manifest.toml and Project.toml

copyproject(joinpath("..", "..", "scripts", "code"))

# Create scripts by merging input scripts inside each folder

createscripts(joinpath("..", "..", "scripts", "code"), "scripts")

# Copy data directory

cp(joinpath("..", "..", "data"), joinpath("data"), force = true)

# Compile scripts to notebooks

createnotebooks("scripts")

