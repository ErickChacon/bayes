# # Bayesian inference
#
# ## Time to capture prey

# ### Load packages

using Distributions
using DataFrames
using Plots
using Turing
using StatsPlots
import CSV
default(dpi = 120, titlefontsize = 11)

path_fig = joinpath("figures", "generated") #src

# ### Read data
#
# Simulated data corresponding to the time fur seals need to capture their first prey.

data = CSV.read("data/simulated/time-to-capture.csv", DataFrame) #src
#nb data = CSV.read("../data/simulated/time-to-capture.csv", DataFrame)
n = nrow(data)

# ### Bayesian inference with conjugate posterior

a, b = 2, 30 ## hyperparametros
prior = InverseGamma(a, b)
posterior = InverseGamma(n + a, b + sum(data.time))

plot(x -> pdf(prior, x), xlim = (0, 70), label = "π(θ)",
    title = "Prior and posterior distributions")
plot!(x -> pdf(posterior, x), label = "π(θ ∣ y)")
savefig(joinpath(path_fig, "03-time-to-capture-naive-posterior.png")) #src

# #### Point estimate

mean(posterior)
#-
var(posterior)

# #### Credible interval

α = 0.1
qq = quantile(posterior, [α/2, 1 - α/2])

# #### Comparison with a classical MLE estimator

est = Normal(mean(data.time), sqrt(var(data.time) / 10))
#-
plot(x -> pdf(posterior, x), label = "π(θ ∣ y)", xlim = (0, 70),
    title = "Comparison with the MLE")
plot!(qq, st = :vline, label = "Credible interval")
plot!(x -> pdf(est, x), label = "MLE")

# #### Excedance probability

plot(x -> pdf(posterior, x), label = "π(θ ∣ y)", xlim = (0, 70),
    title = "Excedance probability"
)
plot!([30], st = :vline, ls = :dash, label = "Threshold")
1 - cdf(posterior, 30)

# #### A function of the parameter

# Distribution of a more complex function $log(\theta)$.

histogram(log.(rand(posterior, 10000)), normalise = true, title = "π(log(θ) ∣ y)",
    label = false, xlab = "log(θ)", ylab = "posterior")

# mean of a more complex function

mean(log.(rand(posterior, 10000))) # adequate
log(mean(posterior)) # wrong

# ### Using the Turing package

# Define the Bayesian model.

@model function bayes_exponential(y, n)
    ## prior
    θ ~ InverseGamma(2, 30)

    ## likelihood
    for i in 1:n
        y[i] ~ Exponential(θ)
    end
end

# Evaluate the model in the data.

m1 = bayes_exponential(data.time, 10)

# Sample from the posterior distribution using MCMC.

chain = Turing.sample(m1, NUTS(0.65), 5000)

# Visualise traceplot and density of the posterior.

plot(chain, lw = 0.4, size = (500, 250))

# Visualise prior and posterior densities.

plot(chain, st = :density, legend = true, label = "Turing")
plot!(x -> pdf(prior, x), xlim = (0, 50), label = "π(θ)",
    title = "Comparison of the analytical result and MCMC")
plot!(x -> pdf(posterior, x), label = "Analytical")

