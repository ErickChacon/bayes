# # Likelihood-based inference
#
# ## Time to capture prey

# ### Load packages

using Distributions
using DataFrames
using Plots
using Random
using Optim
using ForwardDiff
using GLM
import CSV
default(dpi = 200, titlefontsize = 11)

path_fig = joinpath("figures", "generated") #src

# ### Read data
#
# Simulated data corresponding to the time fur seals need to capture their first prey.

data = CSV.read("data/simulated/time-to-capture.csv", DataFrame) #src
#nb data = CSV.read("../data/simulated/time-to-capture.csv", DataFrame)

# Lets obtain some descriptive information about the data.

n = nrow(data)
describe(data)

# Visualize histogram of time to capture prey.

plot(data.time, st = :histogram, nbins = 10, title = "Time-to-capure prey",
    xlab = "time (sec)", ylab = "frequency", lab = false)
plot!([mean(data.time)], st = :vline, lab = "mean")

# ### Log-likelihood function
#
# Assuming the following model
# \begin{equation}
#   Y_i \sim Exponential(\theta).
# \end{equation}
# The parameter $\theta$ represents the mean time-to-capture in the population under
# analysis. The log-likelihood function for the observed data is plotted below.

timeloglike(θ) = sum(logpdf.(Exponential(θ), data.time))
plot(timeloglike, xlim = (9, 100), title = "Log-likelihood function",
    xlab = "θ", ylab = "l(θ)", legend = false)
savefig(joinpath(path_fig, "01-time-to-capture-naive-loglike.png")) #src

# ### Compare theoretical model and empirical data

# Define a function to compare the empirical data with the theoretical model for a
# specific value of the parameter $\theta$.

function plotloglike(θ0)
    p1 = histogram(data.time, nbin = 10, xlim = (0, 150), normalize = true, title = "Comparison",
        label = "Empirical");
    plot!(p1, x -> pdf(Exponential(θ0), x), color = :red, label = "Theoretical")
    p2 = plot(timeloglike, xlim = (9, 100), title = "Log-likelihood function",
        xlab = "θ", ylab = "l(θ)", legend = false);
    scatter!(p2, [θ0], [timeloglike(θ0)], color = :red);
    plot(p1, p2)
end

# The log-likelihood function evaluated at $\theta = 10$ is a low value indicating that
# the theoretical model and the observed data do not seem compatible.

plotloglike(10)
savefig(joinpath(path_fig, "01-time-to-capture-naive-loglike-1.png")) #src

# The log-likelihood function evaluated at $\theta = 80$ is a low value indicating that
# the theoretical model and the observed data do not seem compatible.

plotloglike(80)
savefig(joinpath(path_fig, "01-time-to-capture-naive-loglike-2.png")) #src

# The log-likelihood function evaluated at $\theta = 27$ is a high value indicating that
# the theoretical model and the observed data seem more compatible than with other values
# for $\theta$.

plotloglike(27)
savefig(joinpath(path_fig, "01-time-to-capture-naive-loglike-3.png")) #src

# ### Maximum likelihood estimator

# Obtaining the maximum likelihood estimate analytically.

θ_estimate = mean(data.time)

# Obtaining the maximum likelihood estimate using a optimization method.

initial = [log(10)]
@time out = optimize(x -> -timeloglike(exp(first(x))), initial, Newton(); autodiff = :forward)
estimate = exp.(Optim.minimizer(out))

# ### Confidence interval

Iₒ = ForwardDiff.hessian(x -> timeloglike(first(x)), estimate)
est_var = 1 / (-Iₒ[1,1])
#-
α = 0.05
z = quantile(Normal(0,1), 1 - α/2)
#-
[estimate[1] - sqrt(est_var) * z, estimate[1] + sqrt(est_var) * z]

# It might be better to use the deviance to create confidence intervals for this case.

mydeviance(θ) = 2 * (timeloglike(first(estimate)) - timeloglike(θ))
plot(mydeviance, xlim = (12, 65), label = "Deviance")
hline!([quantile(Chisq(1), 1 - α)], label = "Threshold")

savefig(joinpath(path_fig, "01-time-to-capture-naive-deviance-interval.png")) #src

# ### Using GLM package

glm1 = glm(@formula(time ~ 1), data, Gamma(), LogLink())
#-
exp.(coef(glm1))
#-
confint(glm1)
#-
dispersion(glm1.model)

# ## Time to capture prey with one predictor

# ### Load packages

using Distributions
using DataFrames
using Plots
using Random
using Optim
using ForwardDiff
using GLM
import CSV
default(dpi = 200, titlefontsize = 11)

path_fig = joinpath("figures", "generated") #src

# ### Read data
#
# Simulated data corresponding to the time fur seals need to capture their first prey.

data = CSV.read("data/simulated/time-to-capture.csv", DataFrame) #src
#nb data = CSV.read("../data/simulated/time-to-capture.csv", DataFrame)

# Lets obtain some descriptive information about the data.

n = nrow(data)
describe(data)

# Visualize the relationship of time to capture prey and age.

plot(data.age, data.time, st = :scatter, title = "Time-to-capure prey",
    xlab = "fur seal age (months)", ylab = "time (sec)", lab = false)
savefig(joinpath(path_fig, "01-time-to-capture-lm.png")) #src

# ### Log-likelihood function

# Defining the log-likelihood for the more complex model

timeloglike2(θ) = sum(logpdf.(Exponential.(exp.(θ[1] .+ θ[2] * data.age)), data.time))

# Evaluate the log-likelihood function in a grid of possible values for the paramters.

θ1 = range(24, 25, 200)
θ2 = range(-0.13, -0.105, 200)
θgrid = [[i, j] for i in θ1 for j in θ2]
loglikegrid = reshape(timeloglike2.(θgrid), length(θ2), length(θ1))

plot(θ1, θ2, loglikegrid, st = :contour, fill = true,
    color = cgrad(:RdBu, rev = true), linecolor = :gray,
    title = "Log-likelihood function", xlab = "β₀", ylab = "β₁")
savefig(joinpath(path_fig, "01-time-to-capture-lm-loglike.png")) #src

# ### Maximum likelihood estimator

initial = [21.12, -0.1]
out = optimize(x -> -timeloglike2(x), initial, Newton(); autodiff = :forward)
estimate = Optim.minimizer(out)

# Interpreting the intercept.

exp(estimate[1])

#-

print(minimum(data.age))
exp(estimate[1] + estimate[2] * minimum(data.age))

# Interpreting the slope.

exp(estimate[2])

#-

exp(12 * estimate[2])

# Visualize the estimated mean

mean_time(age) = exp(estimate[1] + estimate[2] * age)

plot(data.age, data.time, st = :scatter, title = "Time-to-capure prey",
    xlab = "fur seal age (months)", ylab = "time (sec)", lab = "data")
plot!(mean_time, lab = "estimated mean")

# ### Confidence intervals

# H = ForwardDiff.hessian(timeloglike2, estimate)

Iₒ = ForwardDiff.hessian(timeloglike2, estimate)
Σ = inv(-Iₒ)

#-
α = 0.05
z = quantile(Normal(0,1), 1 - α/2)
#-
[estimate[1] - sqrt(Σ[1,1]) * z, estimate[1] + sqrt(Σ[1,1]) * z]
#-
[estimate[2] - sqrt(Σ[2,2]) * z, estimate[2] + sqrt(Σ[2,2]) * z]

# ### Using GLM package

glm1 = glm(@formula(time ~ age), data, Gamma(), LogLink())
#-
exp.(coef(glm1))
#-
confint(glm1)
#-
dispersion(glm1.model)

# ## Time to capture prey with predictors

# ### Load packages

using Distributions
using DataFrames
using Chain
using Plots
using Random
using Optim
using ForwardDiff
using GLM
import CSV
default(dpi = 200, titlefontsize = 11)

path_fig = joinpath("figures", "generated") #src

# ### Read data
#
# Simulated data corresponding to the time fur seals need to capture their first prey.

data = CSV.read("data/simulated/time-to-capture.csv", DataFrame) #src
#nb data = CSV.read("../data/simulated/time-to-capture.csv", DataFrame)

# Lets add some ficticious predictors.

n = nrow(data)
data = @chain begin
    transform(data, (x -> randn(n, 3)))
    rename(["x$i" => "pred$i" for i in 1:3])
end

# Lets obtain some descriptive information about the data.

describe(data)

# ### Log-likelihood function

# Defining the log-likelihood for the more complex model
X = Matrix(select(data, :age, r"^pred"))
timeloglike3(θ) = sum(logpdf.(Exponential.(exp.(θ[1] .+ X * θ[2:end])), data.time))

# ### Maximum likelihood estimator

initial = [21.12, -0.1, 0.0, 0.0, 0.0]
out = optimize(x -> -timeloglike3(x), initial, Newton(); autodiff = :forward)
estimate = Optim.minimizer(out)

# ### Confidence intervals

Iₒ = ForwardDiff.hessian(timeloglike3, estimate)
Σ = inv(-Iₒ)

#-
α = 0.05
z = quantile(Normal(0,1), 1 - α/2)
#-
[estimate[1] - sqrt(Σ[1,1]) * z, estimate[1] + sqrt(Σ[1,1]) * z]
#-
[estimate[2] - sqrt(Σ[2,2]) * z, estimate[2] + sqrt(Σ[2,2]) * z]
#-
[estimate[3] - sqrt(Σ[3,3]) * z, estimate[3] + sqrt(Σ[3,3]) * z]
#-
[estimate[4] - sqrt(Σ[4,4]) * z, estimate[4] + sqrt(Σ[4,4]) * z]
#-
[estimate[5] - sqrt(Σ[5,5]) * z, estimate[5] + sqrt(Σ[5,5]) * z]

# ### Using GLM package

glm1 = glm(@formula(time ~ age + pred1 + pred2 + pred3), data, Gamma(), LogLink())
#-
exp.(coef(glm1))
#-
confint(glm1)
#-
dispersion(glm1.model)

