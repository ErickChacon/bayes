\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Rejection sampling}{5}{0}{2}
\beamer@sectionintoc {3}{Gibbs sampling}{8}{0}{3}
\beamer@sectionintoc {4}{Metropolis-Hastings}{11}{0}{4}
