\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Motivation}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Model-based inference}{6}{0}{1}
\beamer@subsectionintoc {1}{3}{Inference}{8}{0}{1}
\beamer@sectionintoc {2}{Likelihood function}{11}{0}{2}
\beamer@sectionintoc {3}{Maximum likelihood estimator}{24}{0}{3}
\beamer@sectionintoc {4}{Asymptotic properties}{29}{0}{4}
\beamer@subsectionintoc {4}{1}{Concepts}{30}{0}{4}
\beamer@subsectionintoc {4}{2}{Properties}{32}{0}{4}
\beamer@subsectionintoc {4}{3}{Confidence intervals}{34}{0}{4}
\beamer@subsectionintoc {4}{4}{Hypothesis testing}{36}{0}{4}
\beamer@subsectionintoc {4}{5}{Log-likelihood ratio test}{37}{0}{4}
