\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Bayesian thinking}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Bayes theorem for the posterior}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Bayesian analysis}{8}{0}{1}
\beamer@subsectionintoc {1}{4}{Example}{9}{0}{1}
\beamer@subsectionintoc {1}{5}{Bayesian model}{10}{0}{1}
\beamer@sectionintoc {2}{Considerations}{14}{0}{2}
