\beamer@sectionintoc {1}{Priors}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Desired Properties}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{Conjugacy}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Mixture of conjugates}{14}{0}{1}
\beamer@subsectionintoc {1}{4}{Noninformative prior}{15}{0}{1}
\beamer@sectionintoc {2}{Model Checking}{28}{0}{2}
\beamer@subsectionintoc {2}{1}{Marginal likelihood}{29}{0}{2}
\beamer@subsectionintoc {2}{2}{Penalised fit criteria}{35}{0}{2}
\beamer@subsectionintoc {2}{3}{Predictive methods}{37}{0}{2}
\beamer@sectionintoc {3}{Hierarquical model}{38}{0}{3}
