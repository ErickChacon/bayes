\beamer@sectionintoc {1}{Generalize linear models}{3}{0}{1}
\beamer@sectionintoc {2}{Generalize linear mixed models}{7}{0}{2}
\beamer@sectionintoc {3}{Survival Analysis}{10}{0}{3}
\beamer@sectionintoc {4}{Markov models}{14}{0}{4}
\beamer@sectionintoc {5}{Point processes}{16}{0}{5}
\beamer@sectionintoc {6}{Text mining}{18}{0}{6}
\beamer@sectionintoc {7}{Longitudinal data analysis}{20}{0}{7}
